import java.util.Scanner;

public class exercici15 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*pera que detecte una tecla */
		Scanner sc=  new Scanner (System.in);
		int va1,va2,in;
		System.out.println("Digues la variable 1:");
		va1 = sc.nextInt();
		System.out.println("Digues la variable 2:");
		va2 = sc.nextInt();
	
		System.out.println(" Variable sense intercanviar:" + va1);
		System.out.println(" Variable sense intercanviar:" + va2);
	
		in = va1;
		va1 = va2;
		va2 = in;
		
		System.out.println(" Variable intercanviada:" + va1);
		System.out.println(" Variable intercanviada:" + va2);
	}}
